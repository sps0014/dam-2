package lectura;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.Iterator;

public class TrabajoExcel {

    public static void leerCelda(File f) {
        FileInputStream fileInputStream = null;
        XSSFWorkbook archivoExcel = null;
        XSSFSheet hojaExcel = null;
        XSSFRow filaExcel = null;
        XSSFCell celda = null;

        try {
            fileInputStream = new FileInputStream(f);
            archivoExcel = new XSSFWorkbook(fileInputStream);
            hojaExcel = archivoExcel.getSheetAt(0);
            filaExcel = hojaExcel.getRow(0);
            celda = filaExcel.getCell(0);
            if (celda.getCellType() == CellType.STRING) {
                System.out.println(celda.getStringCellValue());
            }else if (celda.getCellType() == CellType.NUMERIC) {
            	System.out.println(celda.getNumericCellValue());
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void leerContenidoCompleto(File file) {
        FileInputStream ficheroInput = null;
        XSSFWorkbook ficheroExcel = null;
        XSSFSheet hojaExcel = null;
        XSSFRow fileExcel = null;
        XSSFCell celadaExcel = null;

        try {
            ficheroInput = new FileInputStream(file);
            ficheroExcel = new XSSFWorkbook(ficheroInput);

            Iterator<Sheet> hojaIterator = ficheroExcel.iterator();
            while (hojaIterator.hasNext()) {
                Sheet hoja = hojaIterator.next();
                Iterator<Row> filaIterator = hoja.iterator();
                while (filaIterator.hasNext()) {
                    Row fila = filaIterator.next();
                    Iterator<Cell> celdaIterator = fila.cellIterator();
                    while (celdaIterator.hasNext()) {
                        Cell celda = celdaIterator.next();
                        if (celda.getCellType() == CellType.NUMERIC) {
                            System.out.println(celda.getNumericCellValue());
                        } else if (celda.getCellType() == CellType.STRING) {
                            System.out.println(celda.getStringCellValue());
                        }
                    }
                }
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
