package com.hibernate.modelos;


import java.util.ArrayList;

import java.util.List;
import javax.persistence.*;



@Entity
@Table(name="Proveedor",uniqueConstraints = @UniqueConstraint(columnNames = {"id_proveedor"}))
public class Proveedor {

@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
@Column(name="id_proveedor")
     private int idProveedor;
	 
	 
     
     private String web;
     
     
     private String nombre_proveedor;
     
     
     
     @OneToMany(mappedBy="proveedor")
     private List<Producto> productos = new ArrayList<Producto>();
     
    @OneToMany(mappedBy="proveedor")
     private List<Telefono> telefonos = new ArrayList<Telefono>();

    public Proveedor() {
    }

	
    
    public Proveedor(Telefono telefono, String web, String nombre) {
       
       this.web = web;
       this.nombre_proveedor = nombre;
       
       
    }
   
    
    public int getIdProveedor() {
        return this.idProveedor;
    }
    
    public void setIdProveedor(int idProveedor) {
        this.idProveedor = idProveedor;
    }


    
 

    
    
    public String getWeb() {
        return this.web;
    }
    
    public void setWeb(String web) {
        this.web = web;
    }

    
   
    public String getNombre() {
        return this.nombre_proveedor;
    }
    
    public void setNombre(String nombre) {
        this.nombre_proveedor = nombre;
    }

    
    
    


    public List<Producto> getProductos() {
        return this.productos;
    }
    
    public void setProductos(List<Producto> productos) {
        this.productos = productos;
    }



	public String getNombre_proveedor() {
		return nombre_proveedor;
	}



	public void setNombre_proveedor(String nombre_proveedor) {
		this.nombre_proveedor = nombre_proveedor;
	}



	public List<Telefono> getTelefonos() {
		return telefonos;
	}



	public void setTelefonos(List<Telefono> telefonos) {
		this.telefonos = telefonos;
	}



	@Override
	public String toString() {
		return "Proveedor [idProveedor=" + idProveedor + ", web=" + web + ", nombre_proveedor=" + nombre_proveedor
				+ ", productos=" + productos + ", telefonos=" + telefonos + "]";
	}



	


}


