import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Ficheros_dos {

	public static void main(String[] args) {
		
		File file = new File ("caracteres.txt");
		escribir (file, "hola mundo");
		leer (file);
		

	}
	
	public static void escribir (File file, String texto) {
		FileWriter fileWriter  = null;
		
		try {
			fileWriter= new  FileWriter(file);
		} catch (IOException e) {
			System.err.println("Error en el fileWriter");
			e.printStackTrace();
		}
		
		try {
			fileWriter.write(texto);
			fileWriter.close();
		} catch (IOException e) {
			System.out.println("Error al escribir");
			e.printStackTrace();
		}
	}
	
	public static void leer (File file) {
		
		FileReader fr = null;
		
		try {
			fr  = new FileReader (file);
		} catch (FileNotFoundException e) {
			System.out.println("No se ha podio crear el file reader");
			e.printStackTrace();
		}
		
		int read;
		
		try {
			while((read = fr.read())!=-1) {
				System.out.println((char)read );
			}
			System.out.println();
		} catch (IOException e) {
			System.err.println("Error de lectura");
			e.printStackTrace();
		}
		
		try {
			fr.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
