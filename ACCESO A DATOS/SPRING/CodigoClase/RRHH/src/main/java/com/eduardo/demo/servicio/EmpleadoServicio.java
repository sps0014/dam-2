package com.eduardo.demo.servicio;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.eduardo.demo.modelo.Empleado;
import com.eduardo.demo.repositorio.EmpleadoRepositorio;

@Service
public class EmpleadoServicio implements  IEmpleadoServicio{
	
	@Autowired
	private EmpleadoRepositorio empleadoRepositorio;

	@Override
	public List<Empleado> listarEmpleados() {
		
		return  empleadoRepositorio.findAll();
	}

	@Override
	public Empleado buscarEmpleadoPorId(int idEmpleado) {
		Empleado empleado = empleadoRepositorio.findById(idEmpleado).orElse(null);
		return empleado;
	}

	@Override
	public Empleado guardarEmpleado(Empleado empleado) {
		
		return empleadoRepositorio.save(empleado);
	}

	@Override
	public void eliminarEmpleado(Empleado empleado) {
		
		empleadoRepositorio.delete(empleado);
		
	}
	
	
	
	

}
