package com.eduardo.demo.servicio;
import java.util.List;

import com.eduardo.demo.modelo.Empleado;

public interface IEmpleadoServicio {

	public List<Empleado> listarEmpleados();
	public Empleado buscarEmpleadoPorId(int idEmpleado);
	public Empleado guardarEmpleado(Empleado empleado);
	public void eliminarEmpleado(Empleado empleado);
	
}
