package com.eduardo.demo.config;

import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.provisioning.JdbcUserDetailsManager;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class ConfigSecurity {
	
	@Bean
	public UserDetailsManager userDetailsManager(DataSource datasource) {
		return new JdbcUserDetailsManager(datasource);
	}
	
	@Bean
	public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
		
		http.authorizeHttpRequests(configure ->{
			configure.requestMatchers(HttpMethod.GET, "/rh-app/empleados").hasRole("empleado")
			.requestMatchers(HttpMethod.GET, "/rh-app/empleados/**").hasRole("empleado")
			.requestMatchers(HttpMethod.GET, "/rh-app/empleados/**").hasRole("jefe")
			.requestMatchers(HttpMethod.POST, "/rh-app/empleados").hasRole("jefe")
			.requestMatchers(HttpMethod.PUT, "/rh-app/empleados/**").hasRole("jefe")
			.requestMatchers(HttpMethod.DELETE, "/rh-app/empleados/**").hasRole("jefe");
			//.anyRequest().authenticated();
		});
		
		http.httpBasic(Customizer.withDefaults());
		http.csrf(csrf ->csrf.disable());
		
		return http.build();
	}

}
