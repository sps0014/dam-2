package com.example.demo.controlador;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.modelo.Empleado;
import com.example.demo.response.EmpleadoResponseRest;
import com.example.demo.servicio.IEmpleadoService;

@RestController
@RequestMapping("/rh-app")
//localhost:8080/api/rh-app
public class EmpleadoRestController {
	
	@Autowired
	private IEmpleadoService service;
	//localhost:8080/api/rh-app/empleados
	
	@GetMapping("/empleados")
	public ResponseEntity<EmpleadoResponseRest> consultaCat() {
		
		ResponseEntity<EmpleadoResponseRest> response = service.buscarEmpleados();
		return response;
	}
	
	@GetMapping("/empleados/{id}")
	public ResponseEntity<EmpleadoResponseRest> consultaPorId(@PathVariable int id) {
		ResponseEntity<EmpleadoResponseRest> response = service.buscarPorId(id);
		return response;
	}
	
	
	@PostMapping("/empleados")
	public ResponseEntity<EmpleadoResponseRest> crear(@RequestBody Empleado request) {
		ResponseEntity<EmpleadoResponseRest> response = service.crear(request);
		return response;
	}
	
	@PutMapping("/empleados/{id}")
	public ResponseEntity<EmpleadoResponseRest> actualizar (@RequestBody Empleado request, @PathVariable int id) {
		ResponseEntity<EmpleadoResponseRest> response = service.actualizar(request, id);
		return response;
	}
	
	@DeleteMapping("/empleados/{id}")
	public ResponseEntity<EmpleadoResponseRest> eliminar (@PathVariable int id) {
		ResponseEntity<EmpleadoResponseRest> response = service.eliminar(id);
		return response;
	}

}
