package com.example.demo.modelo.dao;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.modelo.Empleado;



public interface IEmpleadoDao extends CrudRepository<Empleado, Integer>{

}
