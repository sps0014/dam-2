package mongo;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoDatabase;

public class Conexion {
	
	
	private String url= "mongodb+srv://eduardodelolmo:admin@cluster0.uqytnzb.mongodb.net/?retryWrites=true&w=majority";
	private String  database= "sample_airbnb";
	private MongoClient  client;

	
	public MongoDatabase connect (String url, String nombreBD) {
		
		// hago conexion
		client= MongoClients.create(url);
		
		// Seleccionar la bbdd
		MongoDatabase database = client.getDatabase(nombreBD);
		return database;
	}
	
	public void disconnect() {
		client.close();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}

	public MongoClient getClient() {
		return client;
	}

	public void setClient(MongoClient client) {
		this.client = client;
	}
	
	
}
