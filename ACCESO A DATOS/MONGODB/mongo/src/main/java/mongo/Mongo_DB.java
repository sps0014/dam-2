package mongo;

import com.mongodb.client.MongoDatabase;

public class Mongo_DB {

	public static void main(String[] args) {
		
		String url= "mongodb+srv://eduardodelolmo:admin@cluster0.uqytnzb.mongodb.net/?retryWrites=true&w=majority";
		String  database= "sample_airbnb";
		
		Conexion conexion = new Conexion();
		MongoDatabase db = conexion.connect(url, database);
		Metodos m = new Metodos();
		
		m.listarCollection(db, "listingsAndReviews");
		m.buscar(db, "listingsAndReviews", "name", "Ribeira Charming Duplex");

	}

}
