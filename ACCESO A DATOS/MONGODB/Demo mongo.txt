
// CONSULTAS COLECCIÓN SAMPLE_AIRBNB
LISTING AND REVIEWS

********************************
Encontrar los que se llamen Ribeira Charming Duplex
*********************************
db.getCollection("listingsAndReviews").find(
{
   name:"Ribeira Charming Duplex"
    }
);
********************************************
Encontrar los que cumplan dos condiciones
******************************************
db.getCollection("listingsAndReviews").find(
    {
        $or: [{
            name: "Ribeira Charming Duplex"
        }, {
            maximum_nights: "1125"
        }]
    }
);
*********************************************
Encontrar los que tengan entre 10 y 20 camas
***************************************
db.getCollection("listingsAndReviews").find(
{
   beds:{$gt:10,$lt:20}
    }
);
******************************************
Insertar documento con nombre prueba
*******************************************
db.getCollection("listingsAndReviews").insertOne(
{name:"prueba"});

*******************************************
Comprobar que esta creado
*******************************************
db.getCollection("listingsAndReviews").find(
    {name:"prueba"}
        
);
******************************************
Modificar el documento añadiendo 5 camas al que se llame prueba
******************************************
db.getCollection("listingsAndReviews").updateOne(
{ name: "prueba" }, { $set: { beds: 5 } })
*******************************************
Borrar el que se llame prueba
*******************************************
db.getCollection("listingsAndReviews").deleteOne(
{name:"prueba"});
