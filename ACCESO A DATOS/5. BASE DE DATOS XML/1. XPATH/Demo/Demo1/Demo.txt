

**********************************
RUTA ABSOLUTA Y RUTA RELATIVA
**********************************

1. /ies/modulos  ---- RUTA ABSOLUTA

2. //modulos     ---- RUTA RELATIVA

3.  //nombre  ----- Todos los nombres del módulo

4. /ies/modulos/modulo/nombre  --- Lo mismo pero en ruta absoluta

*************************************
ATRIBUTO
*************************************
5. //modulo/@id   --- Accedemos a los atributos id

************************************
TUBERÍAS |  --> Permite indicar varios recorridos
*************************************

6. //modulo/nombre | //curso --> Selecciona los nombres y también los cursos.
7. //modulo/nombre | //modulo/@id

************************************
PREDICADO
***********************************
8. //modulo[@id]  --> Selecciona los módulos que tienen id
9. //modulo [1]
10. 9. //modulo [last()]
*************************************
CONDICIONES  +,-,*,and,or,not(),!=,<,>,<=
**************************************

11. //modulo[@id<0377]  --> Selecciona modulos cuyo id sea menor a 0377
12. //modulo[@id=0376 and curso=2]  --> Dos condiciones

********************************************
NODOS
*******************************************

//modulos/node() --> Selecciona todos los nodos que cuelgan de módulos.

*********************************************
TEXT ()  --> Selecciona solo la información, sin etiquetas.
*********************************************
13. //curso/  --> Los cursos con etiquetas.

14. //curso/text()  --> La misma info sin etiquetas



