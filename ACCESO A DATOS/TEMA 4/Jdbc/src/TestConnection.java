import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class TestConnection {

	public static void main(String[] args) {
		
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			String url = "jdbc:mysql://localhost:3306/dam?characterEncoding=utf8";
			
			try {
				Connection con = DriverManager.getConnection(url,"root","admin");
				
				Statement st = con.createStatement();
				String query = "select * from alumnos";
				
				ResultSet rs = st.executeQuery(query);
				
				while (rs.next()) {
					System.out.println("Id: "+ rs.getInt(1)+ " Nombre: "+ rs.getString(2)+ " Edad : "+ rs.getInt(3));
				}
				
				
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		} catch (ClassNotFoundException e) {
			
			e.printStackTrace();
		}

	}

}
