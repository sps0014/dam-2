import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class Entrada {

	public static void main(String[] args) throws SAXException, IOException {
		
		
		// OPTIMIZAR CODIGO Y MODULAR
			 //crearBuilder()
			// listarElementos (NodeList)
			// leerXml(archivo)
		
		Path path = Path.of("test.xml");
		File xml = path.toFile(); // Convertir un path en file;
		
		
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			Document document = builder.parse(xml);
			
			//Tratar el documento
			
			NodeList listaInicial = document.getElementsByTagName("Tests").item(0).getChildNodes();
			
			// LISTAR ELEMENTOS
			
			for(int i = 0; i<listaInicial.getLength();i++) {
				Node node = listaInicial.item(i);
				
				if(node.getNodeType()== Node.ELEMENT_NODE) {
					Element elemento = (Element) node;
					
					// IMPRIME ATRIBUTOS
					String id = elemento.getAttributes().getNamedItem("TestId").getNodeValue();
					System.out.println(node.getNodeName()+ "\t"+ id);
					System.out.println();
					
					// IMPRIME ELEMENTOS
					
					System.out.println("Name"+ getNodo("Name",elemento));
					System.out.println("CommandLine"+ getNodo("CommandLine",elemento));
				}
				
			}
		
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
		}
		
		

	}
	
	public static String getNodo (String etiqueta, Element elem) {
		NodeList nodo = elem.getElementsByTagName(etiqueta).item(0).getChildNodes();
		Node valorNodo = (Node) nodo.item(0);
		
		return valorNodo.getNodeValue(); // devuelve el valor del nodo
	}

}
