package UDP;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

public class Servidor {

	public static void main(String[] args) throws IOException {
		
		final  int PUERTO = 5000;
		byte [] buffer = new byte [1024];
		System.out.println("Iniciando el servidor UDP");
		
		// Creación del socket
		
			DatagramSocket socketUDP = new  DatagramSocket(PUERTO);
			
			while(true) {
				// Preparo  la  respuesta
				DatagramPacket peticion = new DatagramPacket(buffer, buffer.length);
				
				// Recibo la respuesta
				socketUDP.receive(peticion);
				System.out.println("Recibo la información del cliente");
				
				// Convierto lo recibido y muestro el mensaje
				
				String mensaje = new String(peticion.getData());
				System.out.println(mensaje);
				
				
				// Obtengo el puerto y la direccion  de origen
				int  puertoCliente  = peticion.getPort();
				InetAddress direccion = peticion.getAddress();
				
				mensaje = "Hola, buenos días desde el server";
				buffer = mensaje.getBytes();
				
				// Creo el datagrama
				DatagramPacket respuesta = new DatagramPacket(buffer, buffer.length,direccion,puertoCliente);
				System.out.println("Envío la información  al cliente");
				socketUDP.send(respuesta);
			}
			
		

	}

}
