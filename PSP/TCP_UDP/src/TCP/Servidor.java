package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Servidor {

	public static void main(String[] args) throws IOException {
		
		//Creamos el socket servidor
		ServerSocket servidor = null;
		Socket sc = null;
		
		DataInputStream in;
		DataOutputStream out;
		
		//Puerto del servidor
		final int PUERTO= 5000;
	
	servidor= new ServerSocket(5000);
	System.out.println("Servidor iniciado en el puerto "+ PUERTO);
		
		while(true) {
			
			sc= servidor.accept();
			System.out.println("Cliente conectado");
			in = new DataInputStream(sc.getInputStream());
			out = new DataOutputStream(sc.getOutputStream());
			
			// leo el mensaje del cliente
			
			String  mensaje = in.readUTF();
			System.out.println(mensaje);
			
			//Envio mensaje al cliente
			
			out.writeUTF("Hola,  buenos dias desde el servidor");
			
			//Cierro el socket
			sc.close();
			System.out.println("Hasta pronto");
			
		}
		
		
		
		

	}

}
