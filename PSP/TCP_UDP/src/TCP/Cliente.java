package TCP;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class Cliente {

	public static void main(String[] args) {
		
		//Host del  Servidor
		final String HOST= "localhost";
		// Puerto del  servidor
		final int PUERTO = 5000;
		
		DataInputStream in;
		DataOutputStream out;
		
		
		// Creamos el socket para conectarnos con el servidor
		try {
			Socket sc = new Socket(HOST,PUERTO);
			in = new DataInputStream(sc.getInputStream());
			out = new DataOutputStream(sc.getOutputStream());
			
			// Envío al servidor
			out.writeUTF("Hola, desde el cliente");
			String mensaje = in.readUTF();
			System.out.println(mensaje);
			sc.close();
			
			
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
