
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Ejer_argumentos {

    public static void main(String[] args) throws IOException, InterruptedException {
        if (args.length!=2){
            System.out.println("Error. Debe de haber dos argumentos");
            System.exit(0);
        }
        String pr = args[0];
        int veces = Integer.parseInt(args[1]);
        System.out.println("Voy a ejecutar "+ pr +" "+ veces);
        try{
        ProcessBuilder pb = new ProcessBuilder(pr);
        

        for (int i = 0; i < veces; i++) {
            Process process = pb.start();
            System.out.println("...........");
        }
        /*
        Runtime runtime = runtime.getRuntime();
        for (int i = 0; i < args.length; i++) {
        Process process =runtime.exec (args[i);
        System.out.println("...........");
        
        
            Process process = pb.start();
            System.out.println("...........");
        }
        
        */

    }catch (IOException ex){
            System.out.println("error");
    }

}
}
