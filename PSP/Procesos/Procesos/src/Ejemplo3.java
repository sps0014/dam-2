
import java.io.*;


public class Ejemplo3 {

	public static void main(String[] args) throws IOException {

		ProcessBuilder pb = new ProcessBuilder("CMD", "/C", "DIR");
		Process p = pb.start();

		InputStream is = p.getInputStream();

		int c;
		while ((c = is.read()) != -1) {
			System.out.print((char) c);
			
		}
		is.close();
		
		int valorSalida;
		try {
			valorSalida = p.waitFor();
			System.out.println(valorSalida);
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}

	}

}
