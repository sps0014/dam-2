import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class Main {

	public static void main(String[] args) {

		try {

			ProcessBuilder processBuilder = new ProcessBuilder("ping", "www.google.es");

			File ping = new File("ping.txt");
			File pid = new File("ip.txt");

			// processBuilder.redirectOutput(ping);

			// Iniciar el proceso
			Process proceso = processBuilder.start();

			// Obtener el flujo de entrada del proceso
			InputStream inputStream = proceso.getInputStream();

			// Leer la salida del proceso y mostrarla en la consola
			BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
			String linea;
			reader.readLine(); // La primera linea da error.
			linea = reader.readLine();
			String[] palabras = linea.split(" ");
			System.out.println(palabras[4]);
			FileWriter fr = new FileWriter("ip.txt");
			fr.write(palabras[4]);
			fr.close();
			inputStream.close();

			// Esperar a que el proceso termine
			int salida = proceso.waitFor();

			if (salida == 0) {
				ProcessBuilder otroProcesoBuilder = new ProcessBuilder("mspaint.exe");
				Process otroProceso = otroProcesoBuilder.start();

			}
		} catch (IOException | InterruptedException e) {

			e.printStackTrace();
		}
	}
}
