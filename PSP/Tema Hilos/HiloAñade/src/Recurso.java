import java.util.ArrayList;
import java.util.Scanner;

public class Recurso {
	
	private static ArrayList<Integer> datos = new ArrayList<Integer>();

	
	public Recurso () {
		
		
		
	}
	
	public synchronized  int addNumero () {
		System.out.println("Introduce el número en el array");
		
		Scanner sc = new Scanner (System.in);
		int numero = sc.nextInt();
		
		
		datos.add(numero);
		
		if (datos.size()== 4) {
			mostrarNumeros();
		}
		return numero;
		
		
	}
	
	public void mostrarNumeros() {
		System.out.println("Los números del array son: ");
		
		for(int i = 0; i< datos.size(); i++) {
			System.out.println(datos.get(i)+ " - ");
		}
	}
}
