package ejemplo_sincronizacion;

public class Main {

	public static void main(String[] args) {
		
		
		
		Hilo h1 = new Hilo (1);
		Hilo h2 = new Hilo (2);
		Hilo h3 = new Hilo (3);
		
		h1.start();
		try {
			h1.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		h2.start();
		try {
			h2.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		h3.start();
		
		
		System.out.println("Fin del programa");

	}

}
