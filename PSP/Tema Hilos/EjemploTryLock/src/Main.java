
public class Main {

	public static void main(String[] args) {
		
		CajeroAutomatico cajero = new CajeroAutomatico();

		
		Thread t1 = new Thread() {
			@Override
			public void run() {
				cajero.realizarRetiro(500.00);
				
			}
		};
		Thread t2 = new Thread() {
			@Override
			public void run() {
				cajero.realizarRetiro(700.00);
				
			}
		};
		t1.start();
		t2.start();
	}

}
