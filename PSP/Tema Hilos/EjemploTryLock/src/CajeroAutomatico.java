

import java.util.concurrent.locks.*;
public class CajeroAutomatico {
	private double saldo = 1000;
	
	public Lock lock = new ReentrantLock();
	
	
	public void realizarRetiro(double cantidad) {
		
		
		
		lock.lock();  // Adquirimos el bloqueo
		
		if (saldo >= cantidad) {
			saldo -=cantidad;
			System.out.println("Retiro exitoso de "+ cantidad + " . Saldo restante: "+ saldo);
		}else {
			System.out.println("Saldo insuficiente");
		}
		lock.unlock(); // Libero el bloqueo
		
	}

}
