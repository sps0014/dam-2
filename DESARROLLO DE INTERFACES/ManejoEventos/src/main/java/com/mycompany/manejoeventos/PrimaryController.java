package com.mycompany.manejoeventos;


import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;

public class PrimaryController implements Initializable{

    @FXML
    private Button btn2;

    @FXML
    private Button btn3;

    @FXML
    private Button btn1;

    @FXML
    private Button btnAlert;
    
    

    @FXML
    private ComboBox<String> cmb1;
    
    ObservableList<String> combo;

    @FXML
    public void handler(ActionEvent event) {
        
        if( event.getSource().equals(btn1)){
            System.out.println("Has pulsado el botón 1");
        }else if(event.getSource().equals(btn2)){
            System.out.println("Has pulsado el botón 2");
        }else if (event.getSource().equals(btn3)){
            System.out.println("Has pulsado el botón 3");
        }else if  (event.getSource().equals(cmb1)){
            if(cmb1.getSelectionModel().getSelectedItem()=="Programación"){
            System.out.println("Has elegido la opción programación");
        }else if (cmb1.getSelectionModel().getSelectedItem()=="sistemas"){
            System.out.println("Has elegido la opción de sistemas");
        }else{
            System.out.println("Has elegido la opción de acceso a datos");
        }
       }

    }

    @FXML
    public void mostrarAlerta(ActionEvent event) {
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle("Error");
        alert.setContentText("Vas a entrar a una app con virus. Deseas continuar?");
        alert.showAndWait();

    }
    
   /* @FXML
    public void elegirOpcion(ActionEvent event) {
       
            if(cmb1.getSelectionModel().getSelectedItem()=="Programación"){
            System.out.println("Has elegido la opción programación");
        }else if (cmb1.getSelectionModel().getSelectedItem()=="sistemas"){
            System.out.println("Has elegido la opción de sistemas");
        }else{
            System.out.println("Has elegido la opción de acceso a datos");
        }
       

    }*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        
        ObservableList<String> combo = FXCollections.observableArrayList();
        combo.addAll("Programación","sistemas","acceso a datos");
        cmb1.setItems(combo);
    }
    

}