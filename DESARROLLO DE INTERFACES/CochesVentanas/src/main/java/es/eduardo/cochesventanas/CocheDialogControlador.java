/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package es.eduardo.cochesventanas;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;


public class CocheDialogControlador implements Initializable {

    @FXML
    private TextField txtCv;
    @FXML
    private TextField txtModelo;
    @FXML
    private TextField txtMarca;

    @FXML
    private Button btnGuardar;
    @FXML
    private Button btnSalir;

    private Coche coche;

    private ObservableList<Coche> coches;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    public void iniciarAtributos(ObservableList<Coche> coches) {
      
        this.coches = coches;
    }

    @FXML
    private void guardar(ActionEvent event) {

        String marca = this.txtMarca.getText();
        String modelo = this.txtModelo.getText();
        int cv = Integer.parseInt(this.txtCv.getText());
        
        Coche  c = new Coche (marca,modelo,cv);
        
        // Compruebo si el coche existe
        
        if (!coches.contains(c)){
            this.coche = c;
            
            Alert  alert = new Alert (Alert.AlertType.INFORMATION);
            alert.setHeaderText(null);
            alert.setTitle("Información");
            alert.setContentText("Se ha añadido correctamente");
            alert.showAndWait();
            
            // Cerrar ventana
            
            Stage stage = (Stage) this.btnGuardar.getScene().getWindow();
            stage.close();
            
        }else{
             Alert  alert = new Alert (Alert.AlertType.ERROR);
            alert.setHeaderText(null);
            alert.setTitle("Error");
            alert.setContentText("El coche ya existe");
            alert.showAndWait();
            
        }
    }

    @FXML
    private void salir(ActionEvent event) {
    
        Stage stage = (Stage) this.btnSalir.getScene().getWindow();
            stage.close();

    }


    public Coche getCoche() {
        return coche;
    }

}
